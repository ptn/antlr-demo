grammar F1;

/**
    条件表达式

*/


parse
 : block EOF
 ;

block
 : stat*
 ;

stat
 : assignment
 | if_stat
 | while_stat
 | log
 ;

assignment
 : ID ASSIGN expr SCOL?
 ;

log:
    LOG expr
   ;
if_stat
 : IF condition_block (ELSEIF condition_block)* (ELSE stat_block)?
 ;

condition_block
 : expr stat_block
 ;

stat_block
 : OBRACE block CBRACE
 | stat
 ;

while_stat
 : WHILE expr stat_block
 ;

expr
 : op = (SUM | AVERAGE | VARIANCE | STANDARD | MAX | MIN | MEDIAN) LP expr COMMA expr (COMMA expr)* RP  #calculateExpr
 | POW LP expr COMMA expr RP          #powExpr
 | INDEX LP expr COMMA expr RP          #indexExpr
 | NINDEX LP expr RP          #nindexExpr
 | LOGARITHM LP expr COMMA expr       #logarithmExpr
 | NLOGARITHM LP expr RP        #nlogrithmExpr
 | MINUS expr                           #unaryMinusExpr
 | NOT expr                             #notExpr
 | expr op=(MULT | DIV | MOD) expr      #multiplicationExpr
 | expr op=(PLUS | MINUS) expr          #additiveExpr
 | expr op=(LTE | GTE | LT | GT | EQ | NEQ) expr #relationalExpr
 | expr op=(AND | OR) expr              #logicExpr
 | atom                                 #atomExpr
 ;

atom
 : LP expr RP #parExpr
 | (INT | FLOAT)  #numberAtom
 | (TRUE | FALSE) #booleanAtom
 | ID             #idAtom
 | STRING         #stringAtom
 | NIL            #nilAtom
 ;

OR : '||';
AND : '&&';
EQ : '==';
NEQ : '!=';
GT : '>';
LT : '<';
GTE : '>=';
LTE : '<=';
PLUS : '+';
MINUS : '-';
MULT : '*';
DIV : '/';
MOD : '%';

NOT : '!';

SCOL : ';';
ASSIGN : '=';
LP : '(';
RP : ')';
OBRACE : '{';
CBRACE : '}';
COMMA : ',';

TRUE : 'true';
FALSE : 'false';
IF : '如果';
ELSEIF : '否则如果';
ELSE : '否则';
WHILE : 'while';
NIL : 'nil';
LOG : 'log';

//定义若干高级计算函数

SUM : 'sum';
AVERAGE : 'average';
VARIANCE : 'variance';
STANDARD : 'standard';
MAX : 'max';
MIN : 'min';
MEDIAN : 'median';
POW : 'pow';
INDEX : 'index';
NINDEX : 'nindex';
LOGARITHM : 'logarithm';
NLOGARITHM : 'nlogarithm';



ID
 : [a-zA-Z_] [a-zA-Z_0-9]*
 ;

INT
 : [0-9]+
 ;

FLOAT
 : [0-9]+ '.' [0-9]*
 | '.' [0-9]+
 ;

STRING
 : '"' (~["\r\n] | '""')* '"'
 ;

COMMENT
 : '#' ~[\r\n]* -> skip
 ;


SPACE
 : [ \t\r\n] -> skip
 ;

