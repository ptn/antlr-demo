// Generated from /Users/qiangzi/workspace/job/java-demo/src/main/java/cn/antlr/F1.g4 by ANTLR 4.5.3
package gen.f01;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link F1Parser}.
 */
public interface F1Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link F1Parser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(F1Parser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(F1Parser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link F1Parser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(F1Parser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(F1Parser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link F1Parser#stat}.
	 * @param ctx the parse tree
	 */
	void enterStat(F1Parser.StatContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#stat}.
	 * @param ctx the parse tree
	 */
	void exitStat(F1Parser.StatContext ctx);
	/**
	 * Enter a parse tree produced by {@link F1Parser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(F1Parser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(F1Parser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link F1Parser#log}.
	 * @param ctx the parse tree
	 */
	void enterLog(F1Parser.LogContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#log}.
	 * @param ctx the parse tree
	 */
	void exitLog(F1Parser.LogContext ctx);
	/**
	 * Enter a parse tree produced by {@link F1Parser#if_stat}.
	 * @param ctx the parse tree
	 */
	void enterIf_stat(F1Parser.If_statContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#if_stat}.
	 * @param ctx the parse tree
	 */
	void exitIf_stat(F1Parser.If_statContext ctx);
	/**
	 * Enter a parse tree produced by {@link F1Parser#condition_block}.
	 * @param ctx the parse tree
	 */
	void enterCondition_block(F1Parser.Condition_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#condition_block}.
	 * @param ctx the parse tree
	 */
	void exitCondition_block(F1Parser.Condition_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link F1Parser#stat_block}.
	 * @param ctx the parse tree
	 */
	void enterStat_block(F1Parser.Stat_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#stat_block}.
	 * @param ctx the parse tree
	 */
	void exitStat_block(F1Parser.Stat_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link F1Parser#while_stat}.
	 * @param ctx the parse tree
	 */
	void enterWhile_stat(F1Parser.While_statContext ctx);
	/**
	 * Exit a parse tree produced by {@link F1Parser#while_stat}.
	 * @param ctx the parse tree
	 */
	void exitWhile_stat(F1Parser.While_statContext ctx);
	/**
	 * Enter a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAtomExpr(F1Parser.AtomExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAtomExpr(F1Parser.AtomExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpr(F1Parser.AdditiveExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpr(F1Parser.AdditiveExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relationalExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpr(F1Parser.RelationalExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relationalExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpr(F1Parser.RelationalExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code indexExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterIndexExpr(F1Parser.IndexExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code indexExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitIndexExpr(F1Parser.IndexExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNotExpr(F1Parser.NotExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNotExpr(F1Parser.NotExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nlogrithmExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNlogrithmExpr(F1Parser.NlogrithmExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nlogrithmExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNlogrithmExpr(F1Parser.NlogrithmExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryMinusExpr(F1Parser.UnaryMinusExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryMinusExpr(F1Parser.UnaryMinusExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code logicExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterLogicExpr(F1Parser.LogicExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code logicExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitLogicExpr(F1Parser.LogicExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicationExpr(F1Parser.MultiplicationExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicationExpr(F1Parser.MultiplicationExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code calculateExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCalculateExpr(F1Parser.CalculateExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code calculateExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCalculateExpr(F1Parser.CalculateExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code powExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPowExpr(F1Parser.PowExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code powExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPowExpr(F1Parser.PowExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code logarithmExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterLogarithmExpr(F1Parser.LogarithmExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code logarithmExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitLogarithmExpr(F1Parser.LogarithmExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nindexExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNindexExpr(F1Parser.NindexExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nindexExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNindexExpr(F1Parser.NindexExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void enterParExpr(F1Parser.ParExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void exitParExpr(F1Parser.ParExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNumberAtom(F1Parser.NumberAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNumberAtom(F1Parser.NumberAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code booleanAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void enterBooleanAtom(F1Parser.BooleanAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code booleanAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void exitBooleanAtom(F1Parser.BooleanAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void enterIdAtom(F1Parser.IdAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void exitIdAtom(F1Parser.IdAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void enterStringAtom(F1Parser.StringAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void exitStringAtom(F1Parser.StringAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nilAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNilAtom(F1Parser.NilAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nilAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNilAtom(F1Parser.NilAtomContext ctx);
}