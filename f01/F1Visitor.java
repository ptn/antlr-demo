// Generated from /Users/qiangzi/workspace/job/java-demo/src/main/java/cn/antlr/F1.g4 by ANTLR 4.5.3
package gen.f01;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link F1Parser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface F1Visitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link F1Parser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(F1Parser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link F1Parser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(F1Parser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link F1Parser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStat(F1Parser.StatContext ctx);
	/**
	 * Visit a parse tree produced by {@link F1Parser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(F1Parser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link F1Parser#log}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog(F1Parser.LogContext ctx);
	/**
	 * Visit a parse tree produced by {@link F1Parser#if_stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stat(F1Parser.If_statContext ctx);
	/**
	 * Visit a parse tree produced by {@link F1Parser#condition_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_block(F1Parser.Condition_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link F1Parser#stat_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStat_block(F1Parser.Stat_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link F1Parser#while_stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_stat(F1Parser.While_statContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomExpr(F1Parser.AtomExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpr(F1Parser.AdditiveExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relationalExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalExpr(F1Parser.RelationalExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code indexExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexExpr(F1Parser.IndexExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpr(F1Parser.NotExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nlogrithmExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNlogrithmExpr(F1Parser.NlogrithmExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinusExpr(F1Parser.UnaryMinusExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logicExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicExpr(F1Parser.LogicExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicationExpr(F1Parser.MultiplicationExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code calculateExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCalculateExpr(F1Parser.CalculateExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code powExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPowExpr(F1Parser.PowExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logarithmExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogarithmExpr(F1Parser.LogarithmExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nindexExpr}
	 * labeled alternative in {@link F1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNindexExpr(F1Parser.NindexExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParExpr(F1Parser.ParExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberAtom(F1Parser.NumberAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanAtom(F1Parser.BooleanAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdAtom(F1Parser.IdAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringAtom(F1Parser.StringAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nilAtom}
	 * labeled alternative in {@link F1Parser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNilAtom(F1Parser.NilAtomContext ctx);
}