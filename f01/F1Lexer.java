// Generated from /Users/qiangzi/workspace/job/java-demo/src/main/java/cn/antlr/F1.g4 by ANTLR 4.5.3
package gen.f01;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class F1Lexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		OR=1, AND=2, EQ=3, NEQ=4, GT=5, LT=6, GTE=7, LTE=8, PLUS=9, MINUS=10, 
		MULT=11, DIV=12, MOD=13, NOT=14, SCOL=15, ASSIGN=16, LP=17, RP=18, OBRACE=19, 
		CBRACE=20, COMMA=21, TRUE=22, FALSE=23, IF=24, ELSEIF=25, ELSE=26, WHILE=27, 
		NIL=28, LOG=29, SUM=30, AVERAGE=31, VARIANCE=32, STANDARD=33, MAX=34, 
		MIN=35, MEDIAN=36, POW=37, INDEX=38, NINDEX=39, LOGARITHM=40, NLOGARITHM=41, 
		ID=42, INT=43, FLOAT=44, STRING=45, COMMENT=46, SPACE=47;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"OR", "AND", "EQ", "NEQ", "GT", "LT", "GTE", "LTE", "PLUS", "MINUS", "MULT", 
		"DIV", "MOD", "NOT", "SCOL", "ASSIGN", "LP", "RP", "OBRACE", "CBRACE", 
		"COMMA", "TRUE", "FALSE", "IF", "ELSEIF", "ELSE", "WHILE", "NIL", "LOG", 
		"SUM", "AVERAGE", "VARIANCE", "STANDARD", "MAX", "MIN", "MEDIAN", "POW", 
		"INDEX", "NINDEX", "LOGARITHM", "NLOGARITHM", "ID", "INT", "FLOAT", "STRING", 
		"COMMENT", "SPACE"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'||'", "'&&'", "'=='", "'!='", "'>'", "'<'", "'>='", "'<='", "'+'", 
		"'-'", "'*'", "'/'", "'%'", "'!'", "';'", "'='", "'('", "')'", "'{'", 
		"'}'", "','", "'true'", "'false'", "'如果'", "'否则如果'", "'否则'", "'while'", 
		"'nil'", "'log'", "'sum'", "'average'", "'variance'", "'standard'", "'max'", 
		"'min'", "'median'", "'pow'", "'index'", "'nindex'", "'logarithm'", "'nlogarithm'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "OR", "AND", "EQ", "NEQ", "GT", "LT", "GTE", "LTE", "PLUS", "MINUS", 
		"MULT", "DIV", "MOD", "NOT", "SCOL", "ASSIGN", "LP", "RP", "OBRACE", "CBRACE", 
		"COMMA", "TRUE", "FALSE", "IF", "ELSEIF", "ELSE", "WHILE", "NIL", "LOG", 
		"SUM", "AVERAGE", "VARIANCE", "STANDARD", "MAX", "MIN", "MEDIAN", "POW", 
		"INDEX", "NINDEX", "LOGARITHM", "NLOGARITHM", "ID", "INT", "FLOAT", "STRING", 
		"COMMENT", "SPACE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public F1Lexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "F1.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\61\u0140\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3"+
		"\4\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\13\3"+
		"\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22"+
		"\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\33"+
		"\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\36\3\36"+
		"\3\36\3\36\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3!\3!\3!\3!\3!"+
		"\3!\3!\3!\3!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3$\3$\3$"+
		"\3$\3%\3%\3%\3%\3%\3%\3%\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3(\3(\3("+
		"\3(\3(\3(\3(\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3*\3*\3*"+
		"\3*\3*\3+\3+\7+\u010b\n+\f+\16+\u010e\13+\3,\6,\u0111\n,\r,\16,\u0112"+
		"\3-\6-\u0116\n-\r-\16-\u0117\3-\3-\7-\u011c\n-\f-\16-\u011f\13-\3-\3-"+
		"\6-\u0123\n-\r-\16-\u0124\5-\u0127\n-\3.\3.\3.\3.\7.\u012d\n.\f.\16.\u0130"+
		"\13.\3.\3.\3/\3/\7/\u0136\n/\f/\16/\u0139\13/\3/\3/\3\60\3\60\3\60\3\60"+
		"\2\2\61\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17"+
		"\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\35"+
		"9\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61\3\2\b\5\2C\\aac|\6"+
		"\2\62;C\\aac|\3\2\62;\5\2\f\f\17\17$$\4\2\f\f\17\17\5\2\13\f\17\17\"\""+
		"\u0148\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2"+
		"\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3"+
		"\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2"+
		"\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2"+
		"/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2"+
		"\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2"+
		"G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3"+
		"\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2"+
		"\2\3a\3\2\2\2\5d\3\2\2\2\7g\3\2\2\2\tj\3\2\2\2\13m\3\2\2\2\ro\3\2\2\2"+
		"\17q\3\2\2\2\21t\3\2\2\2\23w\3\2\2\2\25y\3\2\2\2\27{\3\2\2\2\31}\3\2\2"+
		"\2\33\177\3\2\2\2\35\u0081\3\2\2\2\37\u0083\3\2\2\2!\u0085\3\2\2\2#\u0087"+
		"\3\2\2\2%\u0089\3\2\2\2\'\u008b\3\2\2\2)\u008d\3\2\2\2+\u008f\3\2\2\2"+
		"-\u0091\3\2\2\2/\u0096\3\2\2\2\61\u009c\3\2\2\2\63\u009f\3\2\2\2\65\u00a4"+
		"\3\2\2\2\67\u00a7\3\2\2\29\u00ad\3\2\2\2;\u00b1\3\2\2\2=\u00b5\3\2\2\2"+
		"?\u00b9\3\2\2\2A\u00c1\3\2\2\2C\u00ca\3\2\2\2E\u00d3\3\2\2\2G\u00d7\3"+
		"\2\2\2I\u00db\3\2\2\2K\u00e2\3\2\2\2M\u00e6\3\2\2\2O\u00ec\3\2\2\2Q\u00f3"+
		"\3\2\2\2S\u00fd\3\2\2\2U\u0108\3\2\2\2W\u0110\3\2\2\2Y\u0126\3\2\2\2["+
		"\u0128\3\2\2\2]\u0133\3\2\2\2_\u013c\3\2\2\2ab\7~\2\2bc\7~\2\2c\4\3\2"+
		"\2\2de\7(\2\2ef\7(\2\2f\6\3\2\2\2gh\7?\2\2hi\7?\2\2i\b\3\2\2\2jk\7#\2"+
		"\2kl\7?\2\2l\n\3\2\2\2mn\7@\2\2n\f\3\2\2\2op\7>\2\2p\16\3\2\2\2qr\7@\2"+
		"\2rs\7?\2\2s\20\3\2\2\2tu\7>\2\2uv\7?\2\2v\22\3\2\2\2wx\7-\2\2x\24\3\2"+
		"\2\2yz\7/\2\2z\26\3\2\2\2{|\7,\2\2|\30\3\2\2\2}~\7\61\2\2~\32\3\2\2\2"+
		"\177\u0080\7\'\2\2\u0080\34\3\2\2\2\u0081\u0082\7#\2\2\u0082\36\3\2\2"+
		"\2\u0083\u0084\7=\2\2\u0084 \3\2\2\2\u0085\u0086\7?\2\2\u0086\"\3\2\2"+
		"\2\u0087\u0088\7*\2\2\u0088$\3\2\2\2\u0089\u008a\7+\2\2\u008a&\3\2\2\2"+
		"\u008b\u008c\7}\2\2\u008c(\3\2\2\2\u008d\u008e\7\177\2\2\u008e*\3\2\2"+
		"\2\u008f\u0090\7.\2\2\u0090,\3\2\2\2\u0091\u0092\7v\2\2\u0092\u0093\7"+
		"t\2\2\u0093\u0094\7w\2\2\u0094\u0095\7g\2\2\u0095.\3\2\2\2\u0096\u0097"+
		"\7h\2\2\u0097\u0098\7c\2\2\u0098\u0099\7n\2\2\u0099\u009a\7u\2\2\u009a"+
		"\u009b\7g\2\2\u009b\60\3\2\2\2\u009c\u009d\7\u5984\2\2\u009d\u009e\7\u679e"+
		"\2\2\u009e\62\3\2\2\2\u009f\u00a0\7\u5428\2\2\u00a0\u00a1\7\u521b\2\2"+
		"\u00a1\u00a2\7\u5984\2\2\u00a2\u00a3\7\u679e\2\2\u00a3\64\3\2\2\2\u00a4"+
		"\u00a5\7\u5428\2\2\u00a5\u00a6\7\u521b\2\2\u00a6\66\3\2\2\2\u00a7\u00a8"+
		"\7y\2\2\u00a8\u00a9\7j\2\2\u00a9\u00aa\7k\2\2\u00aa\u00ab\7n\2\2\u00ab"+
		"\u00ac\7g\2\2\u00ac8\3\2\2\2\u00ad\u00ae\7p\2\2\u00ae\u00af\7k\2\2\u00af"+
		"\u00b0\7n\2\2\u00b0:\3\2\2\2\u00b1\u00b2\7n\2\2\u00b2\u00b3\7q\2\2\u00b3"+
		"\u00b4\7i\2\2\u00b4<\3\2\2\2\u00b5\u00b6\7u\2\2\u00b6\u00b7\7w\2\2\u00b7"+
		"\u00b8\7o\2\2\u00b8>\3\2\2\2\u00b9\u00ba\7c\2\2\u00ba\u00bb\7x\2\2\u00bb"+
		"\u00bc\7g\2\2\u00bc\u00bd\7t\2\2\u00bd\u00be\7c\2\2\u00be\u00bf\7i\2\2"+
		"\u00bf\u00c0\7g\2\2\u00c0@\3\2\2\2\u00c1\u00c2\7x\2\2\u00c2\u00c3\7c\2"+
		"\2\u00c3\u00c4\7t\2\2\u00c4\u00c5\7k\2\2\u00c5\u00c6\7c\2\2\u00c6\u00c7"+
		"\7p\2\2\u00c7\u00c8\7e\2\2\u00c8\u00c9\7g\2\2\u00c9B\3\2\2\2\u00ca\u00cb"+
		"\7u\2\2\u00cb\u00cc\7v\2\2\u00cc\u00cd\7c\2\2\u00cd\u00ce\7p\2\2\u00ce"+
		"\u00cf\7f\2\2\u00cf\u00d0\7c\2\2\u00d0\u00d1\7t\2\2\u00d1\u00d2\7f\2\2"+
		"\u00d2D\3\2\2\2\u00d3\u00d4\7o\2\2\u00d4\u00d5\7c\2\2\u00d5\u00d6\7z\2"+
		"\2\u00d6F\3\2\2\2\u00d7\u00d8\7o\2\2\u00d8\u00d9\7k\2\2\u00d9\u00da\7"+
		"p\2\2\u00daH\3\2\2\2\u00db\u00dc\7o\2\2\u00dc\u00dd\7g\2\2\u00dd\u00de"+
		"\7f\2\2\u00de\u00df\7k\2\2\u00df\u00e0\7c\2\2\u00e0\u00e1\7p\2\2\u00e1"+
		"J\3\2\2\2\u00e2\u00e3\7r\2\2\u00e3\u00e4\7q\2\2\u00e4\u00e5\7y\2\2\u00e5"+
		"L\3\2\2\2\u00e6\u00e7\7k\2\2\u00e7\u00e8\7p\2\2\u00e8\u00e9\7f\2\2\u00e9"+
		"\u00ea\7g\2\2\u00ea\u00eb\7z\2\2\u00ebN\3\2\2\2\u00ec\u00ed\7p\2\2\u00ed"+
		"\u00ee\7k\2\2\u00ee\u00ef\7p\2\2\u00ef\u00f0\7f\2\2\u00f0\u00f1\7g\2\2"+
		"\u00f1\u00f2\7z\2\2\u00f2P\3\2\2\2\u00f3\u00f4\7n\2\2\u00f4\u00f5\7q\2"+
		"\2\u00f5\u00f6\7i\2\2\u00f6\u00f7\7c\2\2\u00f7\u00f8\7t\2\2\u00f8\u00f9"+
		"\7k\2\2\u00f9\u00fa\7v\2\2\u00fa\u00fb\7j\2\2\u00fb\u00fc\7o\2\2\u00fc"+
		"R\3\2\2\2\u00fd\u00fe\7p\2\2\u00fe\u00ff\7n\2\2\u00ff\u0100\7q\2\2\u0100"+
		"\u0101\7i\2\2\u0101\u0102\7c\2\2\u0102\u0103\7t\2\2\u0103\u0104\7k\2\2"+
		"\u0104\u0105\7v\2\2\u0105\u0106\7j\2\2\u0106\u0107\7o\2\2\u0107T\3\2\2"+
		"\2\u0108\u010c\t\2\2\2\u0109\u010b\t\3\2\2\u010a\u0109\3\2\2\2\u010b\u010e"+
		"\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010dV\3\2\2\2\u010e"+
		"\u010c\3\2\2\2\u010f\u0111\t\4\2\2\u0110\u010f\3\2\2\2\u0111\u0112\3\2"+
		"\2\2\u0112\u0110\3\2\2\2\u0112\u0113\3\2\2\2\u0113X\3\2\2\2\u0114\u0116"+
		"\t\4\2\2\u0115\u0114\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0115\3\2\2\2\u0117"+
		"\u0118\3\2\2\2\u0118\u0119\3\2\2\2\u0119\u011d\7\60\2\2\u011a\u011c\t"+
		"\4\2\2\u011b\u011a\3\2\2\2\u011c\u011f\3\2\2\2\u011d\u011b\3\2\2\2\u011d"+
		"\u011e\3\2\2\2\u011e\u0127\3\2\2\2\u011f\u011d\3\2\2\2\u0120\u0122\7\60"+
		"\2\2\u0121\u0123\t\4\2\2\u0122\u0121\3\2\2\2\u0123\u0124\3\2\2\2\u0124"+
		"\u0122\3\2\2\2\u0124\u0125\3\2\2\2\u0125\u0127\3\2\2\2\u0126\u0115\3\2"+
		"\2\2\u0126\u0120\3\2\2\2\u0127Z\3\2\2\2\u0128\u012e\7$\2\2\u0129\u012d"+
		"\n\5\2\2\u012a\u012b\7$\2\2\u012b\u012d\7$\2\2\u012c\u0129\3\2\2\2\u012c"+
		"\u012a\3\2\2\2\u012d\u0130\3\2\2\2\u012e\u012c\3\2\2\2\u012e\u012f\3\2"+
		"\2\2\u012f\u0131\3\2\2\2\u0130\u012e\3\2\2\2\u0131\u0132\7$\2\2\u0132"+
		"\\\3\2\2\2\u0133\u0137\7%\2\2\u0134\u0136\n\6\2\2\u0135\u0134\3\2\2\2"+
		"\u0136\u0139\3\2\2\2\u0137\u0135\3\2\2\2\u0137\u0138\3\2\2\2\u0138\u013a"+
		"\3\2\2\2\u0139\u0137\3\2\2\2\u013a\u013b\b/\2\2\u013b^\3\2\2\2\u013c\u013d"+
		"\t\7\2\2\u013d\u013e\3\2\2\2\u013e\u013f\b\60\2\2\u013f`\3\2\2\2\f\2\u010c"+
		"\u0112\u0117\u011d\u0124\u0126\u012c\u012e\u0137\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}