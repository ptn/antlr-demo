package gen.f01.main;

import gen.f01.F1Lexer;
import gen.f01.F1Parser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by qiangzi on 16/12/15.
 */
public class F1Main {

    public static void main(String[] args) throws IOException {

        System.out.println("请输入:");

        F1Lexer f1Lexer = new F1Lexer(new ANTLRInputStream(System.in));
        CommonTokenStream tokens = new CommonTokenStream(f1Lexer);
        F1Parser f1Parser = new F1Parser(tokens);

        ParseTree tree = f1Parser.parse();

        JavaVisitor visitor = new JavaVisitor();


        Map<String,Object> context = new HashMap<>();
        context.put("degree", "本科生");
        context.put("age", "25");
        context.put("industry", "IT");

        visitor.setMemory(translate(context));

        visitor.visit(tree);
        System.out.println(visitor.getMemory());


//        System.out.println(f1Parser.file());
//        printArray(f1Parser.data);
//            System.out.println(f1Parser);
//        F1Parser.ProgContext context = f1Parser.prog();
//        System.out.println(context);

//        ParseTree tree = f1Parser.prog();
//
//        MyVisitor visitor = new MyVisitor();
//
//        visitor.visit(tree);

    }


    public static Map<String,Value> translate(Map<String,Object> param){
        Map<String,Value> result = new HashMap<>();
        for(String key: param.keySet()){
            result.put(key, new Value(param.get(key)));
        }
        return result;
    }


    public static <T> void printArray(List<T> as){
        if(as == null){
            System.out.println("list is null");
            return;
        }
        if(as.isEmpty()){
            System.out.println("list is empty");
            return;
        }
        for(T t : as){
            System.out.print(t);
        }
    }
}


