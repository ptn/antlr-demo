package gen.f01.main;

import gen.f01.F1BaseVisitor;
import gen.f01.F1Parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static gen.f01.F1Lexer.AVERAGE;
import static gen.f01.F1Lexer.MAX;
import static gen.f01.F1Lexer.MEDIAN;
import static gen.f01.F1Lexer.STANDARD;
import static gen.f01.F1Lexer.SUM;
import static gen.f01.F1Lexer.VARIANCE;
import static gen.f01.F1Parser.MIN;

/**
 * Created by qiangzi on 16/12/18.
 */
public class JavaVisitor extends F1BaseVisitor<Value> {


    private Map<String,Value> memory = new HashMap<>();


    private double min = 0.000000001;


    @Override
    public Value visitCalculateExpr(F1Parser.CalculateExprContext ctx) {
        List<Double> values = translate(ctx.expr());

        double result = Double.NaN;
        switch (ctx.op.getType()){
            case SUM:
                result = StatisticUtil.sum(values);
                break;
            case AVERAGE:
                result = StatisticUtil.average(values);
                break;
            case VARIANCE:
                result = StatisticUtil.average(values);
                break;
            case MEDIAN:
                result = StatisticUtil.median(values);
                break;
            case STANDARD:
                result = StatisticUtil.standardDeviation(values);
                break;
            case MAX:
                result = StatisticUtil.max(values);
                break;
            case MIN:
                result = StatisticUtil.min(values);
                break;
        }
        return new Value(result);
    }

    private List<Double> translate(List<F1Parser.ExprContext> exprs){
        List<Double> values = new ArrayList<>();
        for(F1Parser.ExprContext c : exprs){
            values.add(this.visit(c).asDouble());
        }
        return values;
    }

    @Override
    public Value visitIndexExpr(F1Parser.IndexExprContext ctx) {
        List<Double> values = translate(ctx.expr());
        return new Value(StatisticUtil.index(values));
    }

    @Override
    public Value visitLogarithmExpr(F1Parser.LogarithmExprContext ctx) {
        List<Double> values = translate(ctx.expr());
        return new Value(StatisticUtil.index(values));
    }

    @Override
    public Value visitNlogrithmExpr(F1Parser.NlogrithmExprContext ctx) {
        return new Value(StatisticUtil.nlogarithm(this.visit(ctx.expr()).asDouble()));
    }

    @Override
    public Value visitNindexExpr(F1Parser.NindexExprContext ctx) {
        return new Value(StatisticUtil.nindex(this.visit(ctx.expr()).asDouble()));
    }
    /**
     * 高级运算: 幂
     */
    @Override
    public Value visitPowExpr(F1Parser.PowExprContext ctx) {
        List<Double> values = translate(ctx.expr());
        return new Value(StatisticUtil.pow(values));
    }
    /**
     * 变量赋值
     */
    @Override
    public Value visitAssignment(F1Parser.AssignmentContext ctx) {
        String id = ctx.ID().getText();
        Value value = this.visit(ctx.expr());
        return memory.put(id, value);
    }

    /**
     * IF 语句
     *
     * if condition_block
     * (else if condition_block)*
     * (else stat_block)*
     *
     * condition_block:expr stat_block
     */
    @Override
    public Value visitIf_stat(F1Parser.If_statContext ctx) {
        List<F1Parser.Condition_blockContext> condition_blockContextList = ctx.condition_block();
        boolean evaluatedBlock = false;
        for(F1Parser.Condition_blockContext bc : condition_blockContextList){
            Value flag = this.visit(bc.expr());
            if(flag.asBoolean()){
                this.visit(bc.stat_block());
                evaluatedBlock = true;
                break;
            }
        }

        if(!evaluatedBlock && ctx.stat_block() != null){
            this.visit(ctx.stat_block());
        }
        return Value.VOID;
    }

    /**
     * while语句
     */
    @Override
    public Value visitWhile_stat(F1Parser.While_statContext ctx) {
        Value value = this.visit(ctx.expr());
        while(value.asBoolean()){
            this.visit(ctx.stat_block());
            value = this.visit(ctx.expr()); //很重要
        }
        return Value.VOID;
    }

    /**
     * 取反
     */
    @Override
    public Value visitNotExpr(F1Parser.NotExprContext ctx) {
        return new Value(!this.visit(ctx.expr()).asBoolean());
    }

    /**
     *  负数
     */
    @Override
    public Value visitUnaryMinusExpr(F1Parser.UnaryMinusExprContext ctx) {
        return new Value(-this.visit(ctx.expr()).asDouble());
    }

    /**
     * 乘法,除法,取模
     * @param ctx
     * @return
     */
    @Override
    public Value visitMultiplicationExpr(F1Parser.MultiplicationExprContext ctx) {
        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));

        switch (ctx.op.getType()) {
            case F1Parser.MULT:
                return new Value(left.asDouble() * right.asDouble());
            case F1Parser.DIV:
                return new Value(left.asDouble() / right.asDouble());
            case F1Parser.MOD:
                return new Value(left.asDouble() % right.asDouble());
            default:
                throw new RuntimeException("unknown operator: " + ctx.op.getText());
        }

    }

    /**
     * 自动取值
     */
    @Override
    public Value visitAtomExpr(F1Parser.AtomExprContext ctx) {
        return super.visitAtomExpr(ctx);
    }


    /**
     * 与或
     */
    @Override
    public Value visitLogicExpr(F1Parser.LogicExprContext ctx) {
        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));
        switch (ctx.op.getType()){
            case F1Parser.AND:
                return new Value(left.asBoolean() && right.asBoolean());
            case F1Parser.OR:
                return new Value(left.asBoolean() || right.asBoolean());
            default:
                throw new RuntimeException("unknown operator: "  + ctx.op.getText());
        }
    }

    /**
     * 加法和减法
     */
    @Override
    public Value visitAdditiveExpr(F1Parser.AdditiveExprContext ctx) {
        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));

        switch (ctx.op.getType()) {
            case F1Parser.PLUS:
                return new Value(left.asDouble() + right.asDouble());
            case F1Parser.MINUS:
                return new Value(left.asDouble() - right.asDouble());
            default:
                throw new RuntimeException("unknown operator: " + ctx.op.getText());
        }
    }



    /**
     * 关联运算
     */
    @Override
    public Value visitRelationalExpr(F1Parser.RelationalExprContext ctx) {

        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));

        switch (ctx.op.getType()) {
            case F1Parser.LT:
                DataUtil.assertDoubleValues(left, right);
                return new Value(left.asDouble() < right.asDouble());
            case F1Parser.LTE:
                DataUtil.assertDoubleValues(left, right);
                return new Value(left.asDouble() <= right.asDouble());
            case F1Parser.GT:
                DataUtil.assertDoubleValues(left, right);
                return new Value(left.asDouble() > right.asDouble());
            case F1Parser.GTE:
                DataUtil.assertDoubleValues(left, right);
                return new Value(left.asDouble() >= right.asDouble());
            case F1Parser.EQ:
                if(left.isString() && right.isString()){
                    return new Value(left.toString().equals(right.toString()));
                }
                else if(left.isBoolean() && right.isBoolean()){
                    return new Value(left.asBoolean() == right.asBoolean());
                }
                else if(left.isDouble() && right.isDouble()){
                    return new Value(Math.abs(left.asDouble() - right.asDouble()) < min);
                }
                else{
                    throw new RuntimeException("eq is not accept:" + ctx.expr(0).getText()  + ":" + ctx.expr(1).getText());
                }
            case F1Parser.NEQ:
                if(left.isString() && right.isString()){
                    return new Value(!left.equals(right));
                }
                else if(left.isBoolean() && right.isBoolean()){
                    return new Value(left.asBoolean() != right.asBoolean());
                }
                else if(left.isDouble() && right.isDouble()){
                    return new Value(Math.abs(left.asDouble() - right.asDouble()) > min);
                }
                else{
                    throw new RuntimeException("Neq is not accept:" + ctx.expr(0).getText()  + ":" + ctx.expr(1).getText());
                }
            default:
                throw new RuntimeException("unknown operator: " + ctx.op.getText());
        }
    }

    /**
     * 括号计算
     */
    @Override
    public Value visitParExpr(F1Parser.ParExprContext ctx) {
        return this.visit(ctx.expr());
    }

    /**
     * 整数,小数
     */
    @Override
    public Value visitNumberAtom(F1Parser.NumberAtomContext ctx) {
        return new Value(DataUtil.asDouble(ctx.getText()));
    }

    /**
     * 布尔值
     */
    @Override
    public Value visitBooleanAtom(F1Parser.BooleanAtomContext ctx) {
        return new Value(DataUtil.asBoolean(ctx.getText()));
    }

    @Override
    public Value visitLog(F1Parser.LogContext ctx) {
        System.out.println(this.visit(ctx.expr()));
        return Value.VOID;
    }

    /**
     * 变量
     */
    @Override
    public Value visitIdAtom(F1Parser.IdAtomContext ctx) {
        String id = ctx.getText();
        Value value = memory.get(id);
        if(value == null){
            throw new RuntimeException("no_such_variable:" + id);
        }
        return value;
    }

    /**
     * 字符串
     */
    @Override
    public Value visitStringAtom(F1Parser.StringAtomContext ctx) {
        String str = ctx.getText();
        str = str.substring(1, str.length() - 1).replace("\"\"", "\"");
        return new Value(str);
    }

    /**
     * null
     */
    @Override
    public Value visitNilAtom(F1Parser.NilAtomContext ctx) {
        return new Value(null);
    }

    public Map<String, Value> getMemory() {
        return memory;
    }

    public void setMemory(Map<String, Value> memory) {
        this.memory = memory;
    }
}
