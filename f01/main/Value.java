package gen.f01.main;

/**
 * Created by qiangzi on 16/12/18.
 */
public class Value {

    private Object value;

    public final static Value VOID = null;

    public Value() {
    }

    public Value(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }


    public double asDouble(){
        if(value == null){
            return 0;
        }
        return DataUtil.asDouble(value.toString());
    }

    public boolean asBoolean(){
        if(value == null){
            return false;
        }
        return DataUtil.asBoolean(value.toString());
    }

    public String asString(){
        if(value == null){
            return null;
        }
        return value.toString();
    }


    public boolean isDouble(){
        return value != null && value instanceof Double;
    }

    public boolean isBoolean(){
        return value != null && value instanceof Boolean;
    }

    public boolean isString(){
        return value != null && value instanceof String;
    }

    @Override
    public String toString() {
        if(this.isBoolean()){
            return this.asBoolean() + "";
        }
        if(this.isDouble()){
            return this.asDouble() + "";
        }
        return this.asString();
    }
}
