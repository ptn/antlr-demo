//package gen.f01.main;
//
//import gen.f01.F1BaseVisitor;
//import gen.f01.F1Parser;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created by qiangzi on 16/12/18.
// */
//public class MyVisitor extends F1BaseVisitor<Integer> {
//
//    Map<String, Integer> memory = new HashMap<>();
//
//    @Override
//    public Integer visitPrintExpr(F1Parser.PrintExprContext ctx) {
//        int value = visit(ctx.expr());
//        System.out.println(value);
//        return 0;
//    }
//
//    @Override
//    public Integer visitAssign(F1Parser.AssignContext ctx) {
//        String id = ctx.ID().getText();
//        int value = visit(ctx.expr());
//        memory.put(id, value);
//        return value;
//    }
//
//    @Override
//    public Integer visitNewline(F1Parser.NewlineContext ctx) {
//        return super.visitNewline(ctx);
//    }
//
//    @Override
//    public Integer visitPlusMinus(F1Parser.PlusMinusContext ctx) {
//        int left = visit(ctx.expr(0));
//        int right = visit(ctx.expr(1));
//        if(ctx.op.getType() == F1Parser.PLUS){
//            return left + right;
//        }
//        return left - right;
//    }
//
//    @Override
//    public Integer visitParens(F1Parser.ParensContext ctx) {
//        return visit(ctx.expr());
//    }
//
//    @Override
//    public Integer visitMulDiv(F1Parser.MulDivContext ctx) {
//        int left = visit(ctx.expr(0));
//        int right = visit(ctx.expr(1));
//        if(ctx.op.getType() == F1Parser.MUL){
//            return left * right;
//        }
//        else{
//            return left / right;
//        }
//    }
//
//    @Override
//    public Integer visitId(F1Parser.IdContext ctx) {
//        String id = ctx.ID().getText();
//        if(memory.containsKey(id)){
//            return memory.get(id);
//        }
//        return 0;
//    }
//
//    @Override
//    public Integer visitInt(F1Parser.IntContext ctx) {
//        return Integer.parseInt(ctx.INT().getText());
//    }
//}
