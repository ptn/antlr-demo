package gen.f01.main;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.List;

/**
 * Created by qiangzi on 16/12/19.
 *
 * 计算工具类
 */
public class StatisticUtil {


    /**
     * 将List<Double>转为 DescriptiveStatistics
     */
    public static DescriptiveStatistics translate(List<Double> data){
        DescriptiveStatistics statistics = new DescriptiveStatistics();
        for(Double item: data){
            statistics.addValue(item);
        }
        return statistics;
    }
    /**
     * 求和
     */
    public static double sum(List<Double> data){
        return translate(data).getSum();
    }

    /**
     * 求方差
     */
    public static double variance(List<Double> data){
        return translate(data).getVariance();
    }

    /**
     * 求平均数
     */
    public static double average(List<Double> data){
        double total = 0;
        for(Double ds : data){
            total += ds.doubleValue();
        }
        return total / data.size();
    }

    /**
     * 求标准差
     */
    public static double standardDeviation(List<Double> data){
        return translate(data).getStandardDeviation();
    }

    /**
     * 求中位数
     */
    public static double median(List<Double> data){
        return translate(data).getPercentile(50);
    }

    /**
     * 最大值
     */
    public static double max(List<Double> data){
        return translate(data).getMax();
    }
    /**
     * 求最小值
     */
    public static double min(List<Double> data){
        return translate(data).getMin();
    }

    /**
     * 求幂
     */
    public static double pow(List<Double> data){
        return Math.pow(data.get(0), data.get(1));
    }

    /**
     * 求指数
     */
    public static double index(List<Double> data){
        return Math.pow(data.get(1), data.get(0));
    }


    /**
     * 求自然指数
     */
    public static double nindex(double data){
        return Math.pow(Math.E, data);
    }



    /**
     * 求对数log(b^a)
     */
    public static double logarithm(List<Double> data){
        return Math.log(data.get(0)) / Math.log(data.get(1));
    }

    /**
     * 求自然对数log(e^a)
     */
    public static double nlogarithm(double data) {
        return Math.log(data) / Math.log(Math.E);
    }

}
