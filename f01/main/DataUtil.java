package gen.f01.main;

/**
 * Created by qiangzi on 16/12/18.
 */
public class DataUtil {


    public static double asDouble(String s){
        try{
            return Double.parseDouble(s);
        }
        catch (Exception e){
            return 0;
        }
    }

    public static boolean asBoolean(String s){
        try{
            return Boolean.parseBoolean(s);
        }
        catch (Exception e){
            return false;
        }
    }


    public static void assertDoubleValues(Value...vs){
        for(Value v:vs){
            if(v == null){
                throw  new RuntimeException("为null");
            }
            try{
                Double.parseDouble(v.toString());
            }
            catch (Exception e){
                throw new RuntimeException("类型不对,必须为double:" + v.toString());
            }
        }
    }
}
