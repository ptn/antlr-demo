grammar F1;


@parser::members{
    public static List<String> data = new ArrayList();
}

file : (row NL{data.add("\n");})+;

row: (
        STUFF {
            if(!"\n".equals($STUFF.text)){
                data.add($STUFF.text);
            }
        }
    )+;


WS : [ \t] -> skip;

STUFF : ~[ \t\r\n]+;

NL : '\r'?'\n';