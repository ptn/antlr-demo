grammar F1;

/**
    Label Visitor

    8 * ( 3 + 9 - 3 * 2 ) - 8 * 6 = 0

*/

@parser::members{


}

prog :
      stat +;


stat :

    expr NEWLINE             #printExpr
         |
            ID EQUAL expr NEWLINE    #assign
         |
            NEWLINE                  #newline
         ;

expr
    :
        expr op=('*' | '/') expr  #MulDiv
    |
        expr op=('+' | '-') expr  #PlusMinus
    |
        INT                       #int
    |
        ID                        #id
    |
        LP expr RP                #parens
     ;
PLUS
    :
        '+'
    ;
MINUS
    :
        '-'
    ;
MUL
    :
        '*'
    ;
DIV
    :
        '/'
    ;
MOD
    :
        '%'
    ;
EQUAL
    :
        '='
    ;

LP :
    '('
   ;
RP :
    ')'
   ;

LBRACKET
    :
        '{'
    ;

RBRACKET
    :
        '}'
    ;
SEMICOLON
    :
        ';'
    ;


PRINT
    :
        'print'
    ;

NEWLINE
    :
        '\r'?'\n'
    ;
INT
    :
        [0-9]+
    ;
ID  :
    [a-z|A-Z|0-9|_]+
    ;
WS
    :
        [ \t\r\n]+ -> skip
    ;