grammar CSV;
@members {
double x, y; // keep column sums in these fields }
file: row+ {System.out.printf("%f, %f\n", x, y);} ;
row : a=field ',' b=field '\r'? '\n' {
      x += Double.valueOf($a.start.getText());
      y += Double.valueOf($b.start.getText());
      }
;
field
: TEXT
;
TEXT : ~[,\n\r]+ ;



grammar Count;
@header { package foo; }
@members {
int count = 0; }
list
@after {System.out.println(count+" ints");}
: INT {count++;} (',' INT {count++;} )* ;
INT : [0-9]+ ;
WS : [ \r\t\n]+ -> skip ;
