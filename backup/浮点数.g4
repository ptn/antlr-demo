grammar F1;


/**
    浮点数

    正确示例:
        1.90
        0.21
        -3.2
    错误示例:
        00.22
        9.
        -00.99
        -3.


*/



expr returns [float value]
    :
        e=FLOAT {$value = Float.parseFloat($e.text);} (
            '+' e=FLOAT{$value += Float.parseFloat($e.text);}
        |
            '-' e=FLOAT{$value -= Float.parseFloat($e.text);}
        )*
    ;


FLOAT
    :
        MINUS? ('0' | NUM) '.' DIGIT+
    ;


NUM
    :
        PDIGIT DIGIT*
    ;

MINUS
    :
        '-'
    ;

DIGIT
    :
        [0-9]
    ;

PDIGIT
    :
        [1-9]
    ;

COMMENT
    :
        '/*' .*? '*/' -> channel(HIDDEN) // match anything between /* and */
    ;
LINE_COMMENT
    :
        '//' .*? '\r'? '\n' -> skip
    ; // Match "//" stuff '\n'

WS
    :
        [ \t\r\n]+ -> skip
    ;