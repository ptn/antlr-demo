

grammar F2;

expr returns [int value=0]
    :a = INT PLUS b = INT
    {
        int aValue = Integer.parseInt($a.text);
        int bValue = Integer.parseInt($b.text);
        $value = aValue + bValue;
    };

PLUS : '+';
INT : '0'..'9';
WS : [ \t\n\r] -> skip;
