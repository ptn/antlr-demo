grammar F1;


/**
    变量

    正确示例:
        int x
        int y
        x = 10
        y = 20


    错误示例:
       int x
       x = 10
       y = 20 // error

*/


prog : block;


block
locals [
    List<String> symbols = new ArrayList<String>();
]
    :
        '{' (decl assign*)+ '}'
        {System.out.println($block::symbols);}
    ;

decl :
    'int' ID { $block::symbols.add($ID.text);} ';'
    ;

assign
    :
        ID '=' INT ';'
        {
            if(!$block::symbols.contains($ID.text)){
                System.err.println("undefined variable:" + $ID.text);
            }
        }
    |
        block
    ;

ID  :
    [a-z]+
    ;
INT
    :
        [0-9]+
    ;

WS
    :
        [ \t\r\n]+ -> skip
    ;