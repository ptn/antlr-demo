grammar F1;


/**
  计算 3 * ( 2 + 5 )
*/
//expr returns [int value=0]
//    :a = INT PLUS b = INT
//    {
//        int aValue = Integer.parseInt($a.text);
//        int bValue = Integer.parseInt($b.text);
//        $value = aValue + bValue;
//    };

//
//expr
//    :  INT
//    |  expr ( PLUS | MUL INT) *
//    ;


expr returns [int value]
    : e=mult { $value = $e.value; }
    (
        PLUS e=mult { $value += $e.value; }
      |
        MUL e=mult { $value *= $e.value; }
    )*
    ;


mult returns [int value]
    : e = atom{$value = $e.value;} (MUL e = atom { $value *= $e.value; })*
    ;

atom  returns [int value]
    : INT {$value = Integer.parseInt($INT.text);}
    ;


PLUS : '+';
MUL : '*';
INT : '0'..'9';
WS : [ \t\n\r] -> skip;
