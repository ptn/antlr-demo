grammar F1;

/**
    全局方法计算器

    8 * ( 3 + 9 - 3 * 2 ) - 8 * 6 = 0

*/

@parser::members{

    int eval(int left, int op, int right){
        switch(op){
            case PLUS: return left + right;
            case MINUS: return left - right;
            case MUL: return left * right;
            case DIV: return left / right;
            case MOD: return left % right;
        }
        return 0;
    }
}

prog :
        (expr{
            System.out.print($expr.text + "=" + $expr.value);
        }
        )+
     ;

expr returns [int value]
    :
        e=highExpr { $value = $e.value;}
        (
            PLUS a=highExpr{$value = eval($value, PLUS, $a.value);}
            |
            MINUS a=highExpr{$value = eval($value, MINUS, $a.value);}

        )*
     ;

highExpr  returns [int value]
    :
        e=atom{$value = $e.value;}
        (
            MUL e=atom{$value = eval($value, MUL, $e.value);}
          |
            DIV e=atom{$value = eval($value, DIV, $e.value);}
          |
            MOD e=atom{$value = eval($value, MOD, $e.value);}
        )*
    ;

atom returns [int value=0]
    :
        INT{$value = Integer.parseInt($INT.text);}
        |
        LP expr RP { $value = $expr.value;}
    ;

STRING
    :
        '"' ID*? '"'
    ;
TYPEINT
    :
        'int'
    ;

PLUS
    :
        '+'
    ;
MINUS
    :
        '-'
    ;
MUL
    :
        '*'
    ;
DIV
    :
        '/'
    ;
MOD
    :
        '%'
    ;
EQUAL
    :
        '='
    ;

LP :
    '('
   ;
RP :
    ')'
   ;

LBRACKET
    :
        '{'
    ;

RBRACKET
    :
        '}'
    ;
SEMICOLON
    :
        ';'
    ;


PRINT
    :
        'print'
    ;

//NEWLINE
//    :
//        '\r'?'\n'
//    ;
INT
    :
        [0-9]+
    ;
ID  :
    [a-z|A-Z|0-9|_]+
    ;
WS
    :
        [ \t\r\n]+ -> skip
    ;