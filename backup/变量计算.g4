grammar F1;

@header{
import java.util.*;
}
/**
    条件语句
    if

    else


    正确示例:
        int x = 10;
        if ( x > 20 ) {
            System.out.println("X 大于20");
        }
        else{
            System.out.println("X 不大于20");
        }


*/


prog : block;


block
locals [
    Map<String,Integer> result = new HashMap<String,Integer>();
]
    :
        (decl assign*)+
        { System.out.println($block::result);}
    ;


decl
    :
        TYPEINT ID (EQUAL e=expr)?
        {
            if($block::result.containsKey($ID.text)){
                System.err.println("错误:变量" + $ID.text + "重复定义");
            }
            $block::result.put($ID.text,0);
            if($EQUAL != null){
                $block::result.put($ID.text,$e.value);
            }
        }
        SEMICOLON
    ;
//
//decls
//    :
//        TYPEINT ID EQUAL e=expr
//        {
//            if(!$block::result.containsKey($ID.text)){
//               $block::result.put($ID.text, $e.value);
//            }
//            else{
//               $block::result.put($ID.text, 0);
//            }
//        }
//        SEMICOLON
//    ;
//
//decl
//    :
//        TYPEINT ID EQUAL e=expr
//        {
//            if(!$block::result.containsKey($ID.text)){
//                System.err.println("undefined variable:" + $ID.text);
//            }
//            else{
//                $block::result.put($ID.text,$e.value);
//            }
//        }
//        SEMICOLON
//    ;


assign
    :
        ID EQUAL e=expr
        {
            if(!$block::result.containsKey($ID.text)){
                System.err.println("undefined variable:" + $ID.text);
            }
            else{
                $block::result.put($ID.text,$e.value);
            }
        }
        SEMICOLON
    ;

expr  returns [int value]
    :
        e=atom{$value = $e.value;}
        (
            PLUS e=atom{$value += $e.value;}
          |
            MINUS e=atom{$value -= $e.value;}
        )*
    ;

atom returns [int value=0]
    :
        INT{$value = Integer.parseInt($INT.text);}
        |
        ID{
            Integer v = $block::result.get($ID.text);
            if(v == null){
               System.err.println("undefined variable:" + $ID.text);
            }
            else{
                $value = v;
            }
        }
    ;

TYPEINT
    :
        'int'
    ;

PLUS
    :
        '+'
    ;
MINUS
    :
        '-'
    ;
EQUAL
    :
        '='
    ;

LBRACKET
    :
        '{'
    ;

RBRACKET
    :
        '}'
    ;
SEMICOLON
    :
        ';'
    ;
ID  :
    [a-z]+
    ;
INT
    :
        [0-9]+
    ;
WS
    :
        [ \t\r\n]+ -> skip
    ;