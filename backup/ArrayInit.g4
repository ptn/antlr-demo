grammar ArrayInit;

init : '{' value ( ',' value) * '}';

value : a=init{System.out.println($a.text);} | b=INT {System.out.println("found a value");};

INT : [0-9]+;

WS : [ \t\r\n]+ -> skip;
