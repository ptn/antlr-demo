grammar F1;


@parser::members{
    int col;
    public F1Parser(TokenStream input, int col){
        this(input);
        this.col = col;
    }
}

file : (row NL)+;

row
locals [int i=0]
: (
        STUFF {
            $i ++;
            if($i == col) {
                System.out.println($STUFF.text);
            }
        }
    )+;


WS : [ \t] -> skip;

STUFF : ~[ \t\r\n]+;

NL : '\r'?'\n';