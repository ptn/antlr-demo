grammar F1;


/**
  计算 3 * ( 2 + 5 )
*/
//expr returns [int value=0]
//    :a = INT PLUS b = INT
//    {
//        int aValue = Integer.parseInt($a.text);
//        int bValue = Integer.parseInt($b.text);
//        $value = aValue + bValue;
//    };

//
//expr
//    :  INT
//    |  expr ( PLUS | MUL INT) *
//    ;


expr returns [int value]
    :
        e=hignExpr { $value = $e.value; }
        (
            PLUS e=hignExpr { $value += $e.value; }
          |
            MINUS e=hignExpr { $value -= $e.value; }
        )*
    ;

hignExpr returns [int value]
    :
       e=atom { $value = $e.value; }
       (
          MUL e = atom { $value *= $e.value; }
          |
          DIV e = atom {
               int val = $e.value;
               if(val == 0){
                   System.err.println("语法错误:除法运算中,除数不能为0");
               }
               else{
                   $value /= val;
               }
           }
           |
           MOD e = atom {
                int val = $e.value;
                if(val == 0){
                    System.err.println("语法错误:取模运算中,除数不能为0");
                }
                else{
                    $value %= val;
                }
            }
           |
           PLUS e=hignExpr { $value += $e.value; }
           |
           MINUS e=hignExpr { $value -= $e.value; }
        )*
    ;

atom  returns [int value]
    :
        INT {$value = Integer.parseInt($INT.text);}
    |
        (LPATRN e=expr{$value = $e.value;} RPATRN)+
    ;


LPATRN
    :
        '('
    ;
RPATRN
    :
        ')'
    ;
MINUS
    :
       '-'
    ;
DIV
    :
       '/'
    ;
MOD
    :
       '%'
    ;
PLUS
    :
       '+'
    ;
MUL
    :
       '*'
    ;
INT
    :
       '0'..'9'+
    ;
WS
    :
       [ \t\n\r] -> skip
    ;
