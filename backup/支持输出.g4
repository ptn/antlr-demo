grammar F1;

/**
    输出语句

    print('ab');
    print('cd');
    print(1234);


*/


prog : print +;

print :
    PRINT LP (STRING | expr) RP {
        if($STRING == null){
            System.out.println($expr.value);
        }
        else{
            System.out.println($STRING.text);
        }
    }
    SEMICOLON
    ;

expr  returns [int value]
    :
        e=atom{$value = $e.value;}
        (
            PLUS e=atom{$value += $e.value;}
          |
            MINUS e=atom{$value -= $e.value;}
        )*
    ;

atom returns [int value=0]
    :
        INT{$value = Integer.parseInt($INT.text);}
    ;

STRING
    :
        '"' ID*? '"'
    ;
TYPEINT
    :
        'int'
    ;

PLUS
    :
        '+'
    ;
MINUS
    :
        '-'
    ;
EQUAL
    :
        '='
    ;

LP :
    '('
   ;
RP :
    ')'
   ;

LBRACKET
    :
        '{'
    ;

RBRACKET
    :
        '}'
    ;
SEMICOLON
    :
        ';'
    ;


PRINT
    :
        'print'
    ;

INT
    :
        [0-9]+
    ;
ID  :
    [a-z|A-Z|0-9|_]+
    ;
WS
    :
        [ \t\r\n]+ -> skip
    ;