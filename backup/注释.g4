grammar F1;


/**
   注释
*/



expr
    :
        INT (
            '+' INT
        |
            '-' INT
        )*
    ;

INT
    :
        [0-9]+
    ;





COMMENT
    :
        '/*' .*? '*/' -> channel(HIDDEN) // match anything between /* and */
    ;
LINE_COMMENT
    :
        '//' .*? '\r'? '\n' -> skip
    ; // Match "//" stuff '\n'

WS
    :
        [ \t\r\n]+ -> skip
    ;