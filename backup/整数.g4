grammar F1;


/**
   支持负数,然后对整数格式进行判断
*/

expr returns [int value]
    :
        e=NUM {$value = Integer.parseInt($e.text);} (
            '+' e=NUM{$value += Integer.parseInt($e.text);}
        |
            '-' e=NUM{$value -= Integer.parseInt($e.text);}
        )*
    ;

NUM
    :
        MINUS? PDIGIT DIGIT*
    ;

MINUS
    :
        '-'
    ;

DIGIT
    :
        [0-9]
    ;

PDIGIT
    :
        [1-9]
    ;
WS
    :
        [ \t\r\n]+ -> skip
    ;